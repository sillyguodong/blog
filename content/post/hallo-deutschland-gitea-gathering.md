---
date: 2024-05-22T10:10:00+00:00
authors:
  - "techknowlogick"
title: "Hallo Deutschland, Gitea gathering in Germany"
tags: ["events"]
draft: false
coverImage: /img/merge_berlin.jpg
---

The Gitea project is excited to announce a partnership[^1] with [THE MERGE](https://merge.berlin), a developer experience conference brought to you by [GitButler](https://gitbutler.com/). Through this partnership we are able to offer **significantly** discounted tickets (€99/ticket) for the Gitea community.  Cocktails and food are all included in the ticket price!

Come and enjoy talks from technical founders and execs from companies such as GitHub, Sentry, Heroku, and Tauri where they'll discuss all things developer tools and the communities they built around them.

With many Gitea maintainers from Germany, we'd like to also thank [CommitGo](https://commitgo.com) for sponsoring the tickets for Gitea maintainers in Germany to be able to attend the conference and to see each other in-person.

THE MERGE is happening June 13 & 14 in Berlin. Use the code [Gitea99](https://ti.to/the-merge/2024/discount/Gitea99) at checkout. There are **only 50 discounted tickets available** to the Gitea community so don't sit on this!

[^1]: This is an informal arrangement where GitButler has shared the above discount code for us to share to benefit the wider OSS community, and those who may otherwise be unable to attend the conference.
