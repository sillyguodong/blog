---
date: 2022-10-30T18:25:00+09:15
authors: "lunny"
title: "A message from Lunny on Gitea Ltd. and the Gitea project"
tags: ["announcement"]
draft: false
---

Thank you everyone for your comments and considerations over the past couple of days regarding our announcement on October 25, 2022. We know the Gitea community is involved and passionate about our shared project and that’s why we’ve been taking the time to listen to your feedback, and I personally would like to address your concerns regarding its future.

## Our beginnings

In 2015, I created Gitea along with its domain, gitea.io and acquired gitea.com at the same time. Throughout the life of the project, I have always personally owned both domains. As it continued to grow, I additionally trademarked the name “Gitea” in order to protect the project’s brand. (More on the trademark later.)

Over the years, Gitea has matured into a robust community with over a thousand contributors globally. However, as with most open source projects, our maintainers have been mainly unpaid volunteers who have spent countless hours of their personal time to manage and grow Gitea. To support the sustainability of the project, we began creating a more formalized operating model so that Gitea could earn funding through public donations, selling Gitea merchandise, and receiving paid commercial contributions. We also ensured that those funds were then evenly distributed to cover basic overhead costs, while also reinvesting back into our maintainers so they could receive minor compensation for their time and effort. While this model has covered the barebones cost for operating, it is not enough to adequately and fairly compensate our maintainers for their time. As well, many commercial entities have been unable to work with individual Gitea maintainers as they often award contracts to organizations instead.

## Our path forward

As we look towards the future of this project, we want to be able to bring in greater opportunities and attract an even broader pool of contributors so Gitea can truly thrive. That includes hiring both part-time and full-time maintainers to be part of the Gitea community so that the project gains dedicated talent and expertise solely focused on further developing Gitea. In order to support this vision, I took the next step to create Gitea Limited.

To keep a transparent governing structure for Gitea, the owners group will continue to vote on decisions about the distribution of funds and the direction of the project. We are working with our current maintainers to ensure that the governance will best support Gitea. I have also transferred both the domains and trademarked name to Gitea Ltd. so that they are no longer personally owned by me and will remain indefinitely with the Gitea project. The logo has always and will continue to be owned by the community.

## Our operational plan

As part of building our business, we will introduce and test new methods for generating revenue for the Gitea project. This will include consulting and contracting arrangements, as well as sponsored development. This will enable us to invest the funds into bounties and other developer contributions, further benefiting the Gitea community.

Our goal remains that we will always give back to the Gitea community whenever possible. We have already been able to contribute back code we’ve made for clients into the codebase, so everyone can access the new developments. We are also currently working on a long-term support (LTS) version in order to provide a stable release for those who need it. As well, we are enhancing Gitea’s vulnerability disclosure program to ensure that security issues are reported and fixed in a timely manner.

## Our continued community engagement

To help ensure the operation and community decision-making of the Gitea project remains transparent and public, we’re exploring the use of different management models. One of the options we have been considering includes a decentralized autonomous organization (DAO). This method would allow us to have continued voting within our community (including votes from non-code contributors), help keep track of topics being voted upon and provides contributors with greater participation to have more votes. The DAO management model would also not mean the creation of a gitcoin or crypto token. While we continue to deliberate how to best oversee and manage the Gitea project going forward, we will keep listening to your feedback as part of our discussions.

We want to be clear that Gitea will always be a community-built project that is open. This means:

* the Gitea project source code will remain open sourced, MIT licensed, and available to everyone
* Gitea Ltd. will be open to building special versions for special clients and will contribute any features back to the main repository when possible
* Gitea Ltd. will always prefer and recommend contracts that align with that project’s mission and values

This is just the start of our new chapter for Gitea and we want to continue engaging with our community members. As our commitment to the community, we will also provide quarterly updates on our progress and will continue to be transparent about our business plan to remain accountable to our contributors. We are always open to your feedback and welcome new ideas to help better the Gitea project. 

Sincerely,

Lunny and techknowlogick
