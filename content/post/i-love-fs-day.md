---
date: 2024-02-14T10:10:00+00:00
authors: "techknowlogick"
title: "Celebrating Free Software with Gitea"
tags: []
draft: false
---

Today marks "I Love Free Software Day", an initiative by the Free Software Foundation Europe (FSFE) to express gratitude to free software contributors. In this spirit, we extend our deepest thanks to the vibrant community around Gitea, whose contributions continually enrich this project.

<!-- more -->

We'd love to spotlight some of the organizations using Gitea to build Free or otherwise Open Source software:

* FSFE itself has embraced Gitea at git.fsfe.org, fostering collaboration among its members. Their invaluable advice and feedback have been appreciated over the years.

* The Blender Foundation utilizes Gitea at projects.blender.org, not only for developing Blender but also for its supporting software. We are thankful for their contributions through pull requests and other advice/support.

* OpenSUSE/SUSE uses Gitea at src.opensuse.org and has contributed to Gitea through pull requests.

* Rocky Linux/RESF hosts an array of projects on git.resf.org, and we are thankful for the support and guidance provided to Gitea.

* OpenStack uses Gitea for opendev.org as a user interface for its many software projects, aiding in the building of OpenStack itself.

We encourage everyone to explore these projects, contribute to them, or start your own. Gitea's journey is enriched by the diverse applications and contributions from these organizations, and many others.

For more insights into "I Love Free Software Day" visit [FSFE's dedicated page](https://fsfe.org/activities/ilovefs/).