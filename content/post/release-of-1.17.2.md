---
date: 2022-09-06T16:35:40+07:00
authors: "jolheiser"
title: "Gitea 1.17.2 is released"
tags: ["release", "gitea"]
draft: false
coverImageRelease: "1.17.2"
---

We are proud to present the release of Gitea version 1.17.2.

We highly encourage users to update to this version for some important bug-fixes.

We have merged [42](https://github.com/go-gitea/gitea/pulls?q=is%3Apr+milestone%3A1.17.2+is%3Amerged) pull requests to release this version.

<!-- Security Thanks! -->

Thanks to [@zeripath](https://gitea.com/zeripath) for the security fixes in this release!

You can download one of our pre-built binaries from our [downloads page](https://dl.gitea.com/gitea/1.17.2/) - make sure to select the correct platform! For further details on how to install, follow our [installation guide](https://docs.gitea.com/installation/install-from-binary).


We would also like to thank all of our supporters on [Open Collective](https://opencollective.com/gitea) who are helping to sustain us financially.

**Have you heard? We now have a [swag shop](https://shop.gitea.io)! :shirt: :tea:**

## :exclamation: Breaking change :exclamation:
#### After release we discovered that a template function removal was backported, which may break user templates. ([#20932](https://github.com/go-gitea/gitea/pull/20932))
#### Users may need to update their custom templates to stop using this function.

<!--more-->

## Changelog

## [1.17.2](https://github.com/go-gitea/gitea/releases/tag/v1.17.2) - 2022-09-06

<!-- Changelog Details -->
* SECURITY
  * Double check CloneURL is acceptable ([#20869](https://github.com/go-gitea/gitea/pull/20869)) ([#20892](https://github.com/go-gitea/gitea/pull/20892))
  * Add more checks in migration code ([#21011](https://github.com/go-gitea/gitea/pull/21011)) ([#21050](https://github.com/go-gitea/gitea/pull/21050))
* ENHANCEMENTS
  * Fix hard-coded timeout and error panic in API archive download endpoint ([#20925](https://github.com/go-gitea/gitea/pull/20925)) ([#21051](https://github.com/go-gitea/gitea/pull/21051))
  * Improve arc-green code theme ([#21039](https://github.com/go-gitea/gitea/pull/21039)) ([#21042](https://github.com/go-gitea/gitea/pull/21042))
  * Enable contenthash in filename for dynamic assets ([#20813](https://github.com/go-gitea/gitea/pull/20813)) ([#20932](https://github.com/go-gitea/gitea/pull/20932))
  * Don't open new page for ext wiki on same repository ([#20725](https://github.com/go-gitea/gitea/pull/20725)) ([#20910](https://github.com/go-gitea/gitea/pull/20910))
  * Disable doctor logging on panic ([#20847](https://github.com/go-gitea/gitea/pull/20847)) ([#20898](https://github.com/go-gitea/gitea/pull/20898))
  * Remove calls to load Mirrors in user.Dashboard ([#20855](https://github.com/go-gitea/gitea/pull/20855)) ([#20897](https://github.com/go-gitea/gitea/pull/20897))
  * Update codemirror to 5.65.8 ([#20875](https://github.com/go-gitea/gitea/pull/20875))
  * Rework repo buttons (#20602, [#20718](https://github.com/go-gitea/gitea/pull/20718)) ([#20719](https://github.com/go-gitea/gitea/pull/20719))
* BUGFIXES
  * Ensure delete user deletes all comments ([#21067](https://github.com/go-gitea/gitea/pull/21067)) ([#21068](https://github.com/go-gitea/gitea/pull/21068))
  * Delete unreferenced packages when deleting a package version ([#20977](https://github.com/go-gitea/gitea/pull/20977)) ([#21060](https://github.com/go-gitea/gitea/pull/21060))
  * Redirect if user does not exist on admin pages ([#20981](https://github.com/go-gitea/gitea/pull/20981)) ([#21059](https://github.com/go-gitea/gitea/pull/21059))
  * Set uploadpack.allowFilter etc on gitea serv to enable partial clones with ssh ([#20902](https://github.com/go-gitea/gitea/pull/20902)) ([#21058](https://github.com/go-gitea/gitea/pull/21058))
  * Fix 500 on time in timeline API ([#21052](https://github.com/go-gitea/gitea/pull/21052)) ([#21057](https://github.com/go-gitea/gitea/pull/21057))
  * Fill the specified ref in webhook test payload ([#20961](https://github.com/go-gitea/gitea/pull/20961)) ([#21055](https://github.com/go-gitea/gitea/pull/21055))
  * Add another index for Action table on postgres ([#21033](https://github.com/go-gitea/gitea/pull/21033)) ([#21054](https://github.com/go-gitea/gitea/pull/21054))
  * Fix broken insecureskipverify handling in redis connection uris ([#20967](https://github.com/go-gitea/gitea/pull/20967)) ([#21053](https://github.com/go-gitea/gitea/pull/21053))
  * Add Dev, Peer and Optional dependencies to npm PackageMetadataVersion ([#21017](https://github.com/go-gitea/gitea/pull/21017)) ([#21044](https://github.com/go-gitea/gitea/pull/21044))
  * Do not add links to Posters or Assignees with ID < 0 ([#20577](https://github.com/go-gitea/gitea/pull/20577)) ([#21037](https://github.com/go-gitea/gitea/pull/21037))
  * Fix modified due date message ([#20388](https://github.com/go-gitea/gitea/pull/20388)) ([#21032](https://github.com/go-gitea/gitea/pull/21032))
  * Fix missed sort bug ([#21006](https://github.com/go-gitea/gitea/pull/21006))
  * Fix input.value attr for RequiredClaimName/Value ([#20946](https://github.com/go-gitea/gitea/pull/20946)) ([#21001](https://github.com/go-gitea/gitea/pull/21001))
  * Change review buttons to icons to make space for text ([#20934](https://github.com/go-gitea/gitea/pull/20934)) ([#20978](https://github.com/go-gitea/gitea/pull/20978))
  * Fix download archiver of a commit ([#20962](https://github.com/go-gitea/gitea/pull/20962)) ([#20971](https://github.com/go-gitea/gitea/pull/20971))
  * Return 404 NotFound if requested attachment does not exist ([#20886](https://github.com/go-gitea/gitea/pull/20886)) ([#20941](https://github.com/go-gitea/gitea/pull/20941))
  * Set no-tags in git fetch on compare ([#20893](https://github.com/go-gitea/gitea/pull/20893)) ([#20936](https://github.com/go-gitea/gitea/pull/20936))
  * Allow multiple metadata files for Maven packages ([#20674](https://github.com/go-gitea/gitea/pull/20674)) ([#20916](https://github.com/go-gitea/gitea/pull/20916))
  * Increase Content field size of gpg_key and public_key to MEDIUMTEXT ([#20896](https://github.com/go-gitea/gitea/pull/20896)) ([#20911](https://github.com/go-gitea/gitea/pull/20911))
  * Fix mirror address setting not working ([#20850](https://github.com/go-gitea/gitea/pull/20850)) ([#20904](https://github.com/go-gitea/gitea/pull/20904))
  * Fix push mirror address backend get error Address cause setting page display error ([#20593](https://github.com/go-gitea/gitea/pull/20593)) ([#20901](https://github.com/go-gitea/gitea/pull/20901))
  * Fix panic when an invalid oauth2 name is passed ([#20820](https://github.com/go-gitea/gitea/pull/20820)) ([#20900](https://github.com/go-gitea/gitea/pull/20900))
  * In PushMirrorsIterate and MirrorsIterate if limit is negative do not set it ([#20837](https://github.com/go-gitea/gitea/pull/20837)) ([#20899](https://github.com/go-gitea/gitea/pull/20899))
  * Ensure that graceful start-up is informed of unused SSH listener ([#20877](https://github.com/go-gitea/gitea/pull/20877)) ([#20888](https://github.com/go-gitea/gitea/pull/20888))
  * Pad GPG Key ID with preceding zeroes ([#20878](https://github.com/go-gitea/gitea/pull/20878)) ([#20885](https://github.com/go-gitea/gitea/pull/20885))
  * Fix SQL Query for `SearchTeam` ([#20844](https://github.com/go-gitea/gitea/pull/20844)) ([#20872](https://github.com/go-gitea/gitea/pull/20872))
  * Fix the mode of custom dir to 0700 in docker-rootless ([#20861](https://github.com/go-gitea/gitea/pull/20861)) ([#20867](https://github.com/go-gitea/gitea/pull/20867))
  * Fix UI mis-align for PR commit history ([#20845](https://github.com/go-gitea/gitea/pull/20845)) ([#20859](https://github.com/go-gitea/gitea/pull/20859))
